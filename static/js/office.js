function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    crossDomain: false,
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


$(document).ready(function () {


    $('.published_note').bind('click', function () {
        var tr = $(this).closest('tr');
        $.ajax({
            type: 'post',
            url: $(this).attr("data-url"),
            success: function (data) {
                obj = JSON.parse(data);
                console.log('====', tr.find('.published a'));
                tr.find('.published a').text(obj['text']);
                if (obj['url'] == null) {
                    tr.find('.url a').detach();
                }
                else {
                    tr.find('.url').append('<a  target="_blank" href="' + obj['url'] + '">' + obj['url'] + '<\a>');
                }
            }
        })
    });

    $('.delete_notes').bind('click', function () {
        var tr = $(this).closest('tr');
        $.ajax({
            type: 'post',
            url: $(this).attr("data-url"),
            success: function () {
                tr.detach()
            }
        })
    })

});