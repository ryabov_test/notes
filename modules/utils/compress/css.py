from compressor.css import CssCompressor as BaseCssCompressor
from django.core.files.base import ContentFile
from django.utils.safestring import mark_safe


class CssCompressor(BaseCssCompressor):
    def output_url(self, mode, content, forced=False, basename=None):
        new_filepath = self.get_filepath(content, basename=basename)
        if not self.storage.exists(new_filepath) or forced:
            self.storage.save(new_filepath, ContentFile(content.encode(self.charset)))
        url = mark_safe(self.storage.url(new_filepath))
        return self.render_output(mode, {"url": url})
