import os
from subprocess import Popen, PIPE

from compressor.filters import FilterBase
from django.conf import settings


class AutoperfixerFilter(FilterBase):
    def output(self, **kwargs):
        p = Popen([settings.AUTOPREFIXER_BINARY], shell=True, stdout=PIPE, stdin=PIPE, stderr=open(os.devnull, 'w'))
        p.stdin.write(bytes(self.content, 'UTF-8'))
        filtered = p.communicate()[0]
        p.stdin.close()
        return filtered.decode('UTF-8')
