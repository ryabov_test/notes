from django.contrib import admin

from ..notes.models import Category, Note


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


admin.site.register(Category, CategoryAdmin)


class NoteAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_date', 'category', 'favorite', 'is_published']
    list_filter = ['category']
    readonly_fields = ['uuid']


admin.site.register(Note, NoteAdmin)
