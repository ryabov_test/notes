from django import forms

from ..notes.models import Note, Category


class CreateNoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['title', 'content', 'category', 'favorite', 'is_published']
        widgets = {
            forms.Textarea(attrs={'class': 'ckeditor'})
        }


def get_category():
    choises = []
    choises.append((0, 'Все'))
    category = Category.objects.all()
    for ct in category:
        choises.append((ct.pk, ct.name))
    return choises


def get_title(user):
    choises = []
    choises.append((0, 'Все'))
    title = Note.objects.filter(user=user)
    for t in title:
        choises.append((t.pk, t.title))
    return choises


def get_favorite():
    choises = [(0, 'Все'), (1, 'Избранные'), (2, 'Не избранные')]
    return choises


class ReportNoterForm(forms.Form):
    def __init__(self, request, *args, **kwargs):
        user = request.user
        super(ReportNoterForm, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.ChoiceField(choices=get_title(user), label='Заголовок', required=False)

    date_from = forms.DateField(label='С даты:', required=False, input_formats=['%d.%m.%Y'])
    date_to = forms.DateField(label='По дату:', required=False, input_formats=['%d.%m.%Y'])
    # TODO: Что произойдет с choices, если в процессе работы добавятся новые категории или изменится избранное?
    category = forms.ChoiceField(choices=get_category(), label='Категория', required=False)
    favorite = forms.ChoiceField(choices=get_favorite(), label='Избранные', required=False)
