from django.conf.urls import url

from modules.notes import views

urlpatterns = [
    url(r'^notes/$', views.notes, name='notes'),
    url(r'^notes/create/$', views.notes_create, name='notes_create'),
    url(r'^notes/update/(?P<uuid>[0-9a-z-]+)/$', views.notes_update, name='notes_update'),
    url(r'^notes/published/(?P<uuid>[0-9a-z-]+)/$', views.notes_published, name='notes_published'),
    url(r'^notes/delete/(?P<uuid>[0-9a-z-]+)/$', views.notes_delete, name='notes_delete'),
    url(r'^notes/(?P<uuid>[0-9a-z-]+)/$', views.notes_view, name='notes_view'),
]
