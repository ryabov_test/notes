# -*- coding: utf-8 -*-
import json

from braces.views import LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.http import HttpResponse
from django.views import generic as cbv

from ..notes.forms import CreateNoteForm
from ..notes.mixins import ReportNoterMixin
from ..notes.models import Note


class Notes(LoginRequiredMixin, ReportNoterMixin, cbv.ListView):
    template_name = 'office/notes.html'
    model = Note
    context_object_name = 'notes'

    def get_queryset(self):
        order_by = self.request.GET.get('order_by', '-created_date')
        user = self.request.user
        queryset = self.model.objects.filter(user=user).order_by(order_by)
        return self.filtred_queryset(queryset)


notes = Notes.as_view()


class CreateNotes(LoginRequiredMixin, cbv.CreateView):
    template_name = 'office/create_or_update_notes.html'
    model = Note
    form_class = CreateNoteForm
    success_url = reverse_lazy('notes')

    def form_valid(self, form):
        print()
        obj = form.save(commit=False)
        obj.user = self.request.user
        return super(CreateNotes, self).form_valid(form)


notes_create = CreateNotes.as_view()


class UpdateNotes(LoginRequiredMixin, cbv.UpdateView):
    template_name = 'office/create_or_update_notes.html'
    model = Note
    form_class = CreateNoteForm
    success_url = reverse_lazy('notes')

    def get_object(self, queryset=None):
        return self.model.objects.get(uuid=self.kwargs.get('uuid'))


notes_update = UpdateNotes.as_view()


class ViewNotes(LoginRequiredMixin, cbv.TemplateView):
    template_name = 'office/view_notes.html'
    model = Note

    def get_context_data(self, **kwargs):
        context = super(ViewNotes, self).get_context_data(**kwargs)
        try:
            node = self.model.objects.get(uuid=self.kwargs.get('uuid'))
            context.update({
                'node': node
            })
        except:
            raise Http404
        return context


notes_view = ViewNotes.as_view()


class DeleteNotes(LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin, cbv.View):
    model = Note
    success_url = reverse_lazy('author-list')

    def post_ajax(self, request, *args, **kwargs):
        note = Note.objects.get(uuid=self.kwargs.get('uuid'))
        note.delete()
        return HttpResponse()

notes_delete = DeleteNotes.as_view()


class PublishedNotes(LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin, cbv.View):

    def post_ajax(self, request, *args, **kwargs):
        note = Note.objects.get(uuid=self.kwargs.get('uuid'))
        # TODO: note.is_published = not note.is_published выглядит проще
        if note.is_published:
            note.is_published = False
        else:
            note.is_published = True
        note.save()
        note = {
            'text': 'Убрать публикацию' if note.is_published else 'Опубликовать',
            'url': note.get_published_url
        }

        return self.render_json_response(json.dumps(note))


notes_published = PublishedNotes.as_view()
