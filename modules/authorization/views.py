# -*- coding: utf-8 -*-
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render
from django.views import generic as cbv

from modules.authorization.forms import AuthenticationForm, RegistrationForm
from modules.utils.generate import generate_random_username


class Login(AnonymousRequiredMixin, cbv.View):
    form = AuthenticationForm
    template_name = 'users/login.html'

    def get(self, request):
        request.session['next'] = request.GET.get('next', None)
        return render(request, self.template_name, {'auth_form': self.form, })

    def post(self, request):
        form = self.form(request.POST)
        response_data = {}
        if form.is_valid():
            auth_login(request, user=form.user)
            response_data['status'] = 'ok'
            _next = request.session.get('next')
            # TODO: Может быть проще return redirect(_next or reverse('notes'))
            if _next is not None:
                return redirect(_next)
            else:
                return redirect(reverse('notes'))
        else:
            return render(request, self.template_name, {'auth_form': form, })


login = Login.as_view()


class Registration(AnonymousRequiredMixin, cbv.View):
    form = RegistrationForm
    template_name = "users/registration.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'reg_form': self.form()})

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            password = form.cleaned_data['reg_password']
            username = generate_random_username()
            user = User.objects.create(first_name=first_name, password=password, email=email, username=username)
            auth_login(request, user=user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect(reverse('notes'))
        else:
            return render(request, self.template_name, {'reg_form': form})


registration = Registration.as_view()


class LogOut(LoginRequiredMixin, cbv.View):

    def get(self, request, *args, **kwargs):
        _next = request.GET.get('next', None)
        auth_logout(request)
        if _next is not None:
            return _next
        return redirect('main')


logout = LogOut.as_view()
