import logging

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from email_validator import validate_email, EmailNotValidError

logger = logging.getLogger(__name__)


class AuthenticationForm(forms.Form):
    user = None
    identification = forms.CharField(label="Логин или Email", max_length=75)
    password = forms.CharField(label='Password', widget=forms.PasswordInput(render_value=False))

    def clean_password(self):
        identification = self.cleaned_data.get('identification')
        password = self.cleaned_data.get('password')
        if identification and password:
            user = authenticate(identification=identification, password=password)
            if user is None:
                raise forms.ValidationError("Введите корректный телефон(email) и пароль")
            else:
                self.user = user

        return self.cleaned_data


class RegistrationForm(forms.Form):
    first_name = forms.CharField(label='Имя', required=True, max_length=30, min_length=2)
    email = forms.EmailField(label='Email', required=True, max_length=50, min_length=6)
    reg_password = forms.CharField(label='Пароль', required=True, widget=forms.PasswordInput, min_length=6)


    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        # TODO: Не нужно путать местами операнды: логически вы не единицу проверяете по отношению к чему-то, а длину по отношению к единице
        if 1 < len(first_name.split(' ')):
            raise forms.ValidationError('Имя не должно содержать пробелы')
        return first_name

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            validate_email(email)
        except EmailNotValidError as e:
            # TODO: Мы заглушили исключение, для чего мы продолжаем работу в функции дальше?
            print(str(e))
        if User.objects.filter(email__iexact=email).count() > 0:
            raise forms.ValidationError('Такой емайл уже зарегестрирован')
        return email
